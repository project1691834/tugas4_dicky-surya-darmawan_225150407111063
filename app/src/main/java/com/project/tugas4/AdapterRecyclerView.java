package com.project.tugas4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {

    private ArrayList<ListPeople> dataItem;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textNama;
        TextView textNo;
        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textNama = itemView.findViewById(R.id.textView);
            textNo = itemView.findViewById(R.id.textView2);
            image = itemView.findViewById(R.id.imageView);
        }
    }

    AdapterRecyclerView(ArrayList<ListPeople> dataItem){
        this.dataItem = dataItem;
    }
    @NonNull
    @Override
    public AdapterRecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listlayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecyclerView.ViewHolder holder, int position) {

        TextView textNama = holder.textNama;
        TextView textNo = holder.textNo;
        ImageView image = holder.image;

        textNama.setText(dataItem.get(position).getName());
        textNo.setText(dataItem.get(position).getId());
    }

    @Override
    public int getItemCount() {
        return dataItem.size();
    }

}
