package com.project.tugas4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager;

    ArrayList<ListPeople> peopleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editText1 = findViewById(R.id.editText1);
        EditText editText2 = findViewById(R.id.editText2);
        Button btnSimpan = findViewById(R.id.btn_simpan);

        recyclerView = findViewById(R.id.recyc1);
        recyclerView.setHasFixedSize(true);

        recyclerViewLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        peopleList = new ArrayList<>();
        for (int i = 0; i < fillPeopleList.textNama.length; i++){
            peopleList.add(new ListPeople(fillPeopleList.textNama[i], fillPeopleList.textNo[i]));
        }

        recyclerViewAdapter = new AdapterRecyclerView(peopleList);
        recyclerView.setAdapter(recyclerViewAdapter);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = editText1.getText().toString();
                String nim = editText2.getText().toString();

                ListPeople newPerson = new ListPeople(nama, nim);

                peopleList.add(newPerson);

                recyclerViewAdapter.notifyDataSetChanged();

                editText1.setText("");
                editText2.setText("");
            }
        });


    }

}